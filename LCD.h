//Lcd
#include <LiquidCrystal.h>
 

String displayUpStrings[4] = { "  ", "  ", " ", "  " };
String displayDownStrings[4] = { "  ", "  ", " ", "  " };
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);





void PrintStrings(int index) {
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(displayDownStrings[index]);
}


void SetConstantValues() {
  displayUpStrings[0] = "Level Sensor ";
  displayUpStrings[1] = "Distance Sensor ";
  displayUpStrings[2] = "Cubic Meters ";
  displayUpStrings[3] = "Level Sensor";
}
void SetContiuniously(int liquidLevel, float distance, float cubicMeters) {
  displayDownStrings[0] = String(liquidLevel);
  displayDownStrings[1] = String(distance, 3);
  displayDownStrings[2] = String(cubicMeters, 3);
}
void PrintConstantValues(int workSpacesIndex) {
  //lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(displayUpStrings[workSpacesIndex]);
}
void PrintContiously(int workSpacesIndex) {

  lcd.setCursor(11, 1);
  lcd.print(displayDownStrings[workSpacesIndex]);
}

void SetNPrint(int workSpacesIndex, int liquidLevel, float distance, float cubicMeters) {
  SetConstantValues();
  SetContiuniously(liquidLevel, distance, cubicMeters);
  PrintConstantValues(workSpacesIndex);
  PrintContiously(workSpacesIndex);
}
void initialize_screen() {
  SetConstantValues();
  PrintConstantValues(1);
}


//Buttons
int workSpaceINT = 0;
enum keys { none,
            select,
            left,
            down,
            up,
            right
};

const int button_id = A0;
int screenButtonsPinAvrg = 0;
keys currentKey = none;
String currentKeyName = "none";



void GetKey(int workSpacesIndex, int liquidLevel, float distance, float cubicMeters) {
  screenButtonsPinAvrg = analogRead(button_id);

  if (screenButtonsPinAvrg >= 818)  ///nothing from4 volts and up
  {
    currentKey = none;
    currentKeyName = "none";
  }
  if (630 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 800) {
    currentKey = select;
    currentKeyName = "select";

    currentKey = none;
  }
  if (400 <= screenButtonsPinAvrg && 600 > screenButtonsPinAvrg) {
    currentKey = left;
    currentKeyName = "left";
    workSpaceINT--;
    if (workSpaceINT < 0) {
      workSpaceINT = 3;
    }
      SetNPrint(workSpaceINT, liquidLevel, distance, cubicMeters);

    currentKey = none;
  }
  if (204 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 300) {
    currentKey = down;
    currentKeyName = "down";
    currentKey = none;
  }
  if (90 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 150) {
    currentKey = up;
    currentKeyName = "up";
    currentKey = none;
  }
  if (screenButtonsPinAvrg < 61) {
    currentKey = right;
    currentKeyName = "right";
    workSpaceINT++;
    if (workSpaceINT > 3) {
      workSpaceINT = 0;
    }
      SetNPrint(workSpaceINT, liquidLevel, distance, cubicMeters);

    currentKey = none;
  }

  delay(150);
}



//Buttons
