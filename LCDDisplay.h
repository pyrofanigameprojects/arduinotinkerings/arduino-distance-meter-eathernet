//Lcd
#include <LiquidCrystal.h>
//  #include "LevelSensor.h"
// #include "DistanceSensor.h"
// #include "CubicMetersCalc.h"

  String displayUpStrings[4] = { "  ", "  ", " ", "  " };
  String displayDownStrings[4] = { "  ", "  ", " ", "  " };
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);





void PrintStrings(int index) {
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(displayDownStrings[index]);
}

void SetUPDisplayStrings(){
  displayUpStrings[0]="Level Sensor ";
     displayUpStrings[1]="Distance Sensor ";
     displayUpStrings[2]="Cubic Meters ";
   displayUpStrings[3]="Eathernet State ";

}
void SetStringsToDisplay(int liquidLevel,float distance,float cubicMeters) {
   displayDownStrings[0]=String(liquidLevel,3); 
  displayDownStrings[1]=String(distance,3); 
 displayDownStrings[2]=String(cubicMeters,3); 
  displayDownStrings[3]="Connected"; 

}
void SetAndPrintContiniouslyUpdatedValues(int workSpacesIndex,int liquidLevel,float distance,float cubicMeters) {
 SetStringsToDisplay(liquidLevel,distance,cubicMeters);
 lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(displayUpStrings[workSpacesIndex]);
  lcd.setCursor(0, 1);
 lcd.print(displayDownStrings[workSpacesIndex]);
}
void initialize_screen() {
  //screen
  lcd.begin(16, 2);
  SetUPDisplayStrings();
SetAndPrintContiniouslyUpdatedValues( 0,0,0,0);

 
}

void Screen_loop(int workSpacesIndex,int liquidLevel,float distance,float cubicMeters) {
  SetAndPrintContiniouslyUpdatedValues(workSpacesIndex,liquidLevel,distance,cubicMeters);
}